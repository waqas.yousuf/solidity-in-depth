// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

error IndexOutOfBounds();

contract Arrays {

    // creating arrays in storage
    uint[] public arr;
    uint[3] public numsFixed;


    function examples() external {
        // delete nums[1];

        // creating array in memory
        // the array in memory has to be a fixed size (only update the element or get the value)
        // delete does not remove the element it just sets its default value 
        // uint[] memory a = new uint[](5);
        // a.pop(); // this line will throw error
    }

    function remove(uint _index) public {
        require(_index < arr.length, "Index out of bounds");
        for (uint i = _index; i < arr.length - 1; i++) {
            arr[i] = arr[i+1];
        }
        arr.pop();
    }


    function getNums() external view returns (uint[] memory) {
        return arr;        
    }

    function test() external {
        arr = [1, 2, 3, 4, 5];
        remove(2);

        assert(arr[3] == 5);
        assert(arr.length == 4);
    }

}

// Function that is returning an array is not recommended at all