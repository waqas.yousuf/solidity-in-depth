// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract CallTestContract {

    // Option 1: initialize the contract and call the method
    // function setX(address _testContract, uint _x) external {
    //     TestContract(_testContract).setX(_x);
    // }

    // Option 2: type cast the contract in the argument and call the method
    function setX(TestContract _testContract, uint _x) external {
        _testContract.setX(_x);
    }

    function getX(TestContract _testContract) external view returns (uint x) {
        x = _testContract.getX();
    }

    function setXandReceiveEther(TestContract _testContract, uint _x) external payable {
        _testContract.setXandReceiveEthers{value: msg.value}(_x);
    }

    function getXandValue(TestContract _testContract) external view returns (uint x, uint value) {
        (x, value) = _testContract.getXandValue();
    }

}

contract TestContract {

    uint public x;
    uint public value = 123;

    function setX(uint _x) external {
        x = _x;
    }

    function getX() external view returns (uint) {
        return x;
    }

    function setXandReceiveEthers(uint _x) external payable {
        x = _x;
        value = msg.value;
    }

    function getXandValue() external view returns (uint, uint) {
        return (x,value);
    }

}