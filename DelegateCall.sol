// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract TestDelegateCall {
    uint public num;
    address public sender;
    uint public value;
    address public owner; // preserve the variable declaration flow in both contracts

    function setVars(uint _num) external payable {
        num = _num;
        sender = msg.sender;
        value = msg.value;
    }
}

contract DelegateCall {
    uint public num;
    address public sender;
    uint public value;

    function setVars(address _test, uint _num) external payable {
        // (bool success, ) = _test.delegatecall(abi.encodeWithSignature("setVars(uint256)", _num));
        (bool success, bytes memory data) = _test.delegatecall(abi.encodeWithSelector(TestDelegateCall.setVars.selector, _num));
        require(success, "delegate call failed");
        
    }
}