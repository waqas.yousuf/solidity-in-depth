// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "hardhat/console.sol";

contract FunctionOutputs {
    function returnMany() public pure returns (uint, bool) {
        return (1, true);
    }

    function named() public pure returns (uint x, bool y) {
        return(1,true);
    }

    function assigned() public pure returns (uint x, bool y) {
        x = 1;
        y = true;
        return (4, false);
    }

    function destructingAssignments() public view {
        (uint a, bool b) = assigned();
        console.log(a, b);
        // (, bool _b) = returnMany();
        // console.log(_b);

    }
}

