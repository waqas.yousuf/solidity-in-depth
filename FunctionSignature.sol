// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


contract FunctionSelector {
    function getSelector(string calldata _func) external pure returns (bytes4) {
        return bytes4(keccak256(bytes(_func)));
    }
}



contract Receiver {
    
    event Log(bytes data);
    
    function transfer(address _account, uint _amount) external {
        emit Log(msg.data);

        /*
        0xa9059cbb // these first four bytes encodes the function to call. These four bytes also known as function selector
        // below rest is the parameters passed to the function
        0000000000000000000000005b38da6a701c568545dcfcb03fcb875f56beddc4 // address that we passed
        00000000000000000000000000000000000000000000000000000000000003e7 // amount that we passed
        */

    }
}

// "transfer(address, uint256)"