// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

/*

# ketchhaakk 256

## Use cases
- create signatures, unique IDs, craete contract to protect from frontrunning called commit reveal scheme

- keccak256 takes the input in bytes

## abi.encodePacked Vs abi.encode
- both of the above encodes the data into bytes but encodePacked does some compression and output will be shorter
- using abi.encodePacked there is a risk of hash collision in case of inputs are of dynamic data type
- in order to avoid the collision do not place dynamic data type next to each other instead put a value data type in between

*/

contract HashFunc {

    function hash(string memory text, uint num, address addr) external pure returns (bytes32) {
        return keccak256(abi.encodePacked(text, num, addr));
    }

    function encode(string memory text0, string memory text1) external pure returns(bytes memory) {
        return abi.encode(text0, text1);
    }

    function encodePacked(string memory text0, string memory text1) external pure returns (bytes memory) {
        return abi.encodePacked(text0, text1);
    }

    function collision(string memory text0, uint x, string memory text1) external pure returns (bytes32) {
        return keccak256(abi.encodePacked(text0, x, text1));
    }

}