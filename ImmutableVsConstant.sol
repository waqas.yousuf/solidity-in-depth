// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract ImmutableVsConstant {

    bool public immutable exists;
    address public owner;
    uint public x;

    constructor(){
        owner = msg.sender;
        exists = true;

    }

    function foo() external {
       require(msg.sender == owner);
       x += 1;
    }

}

// contract A {

//     ImmutableVsConstant public C = new ImmutableVsConstant();

//     function getStr() view external returns (string memory) {
//         string memory myStr = C.str;
//         // return C.str;
//     }


// }