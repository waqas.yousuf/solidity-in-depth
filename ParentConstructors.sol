// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract S {
    string public name;

    constructor(string memory _name) {
        name = _name;
    }

}

contract T {
    string public text;

    constructor(string memory _text) {
        text = _text;
    }
}

// passing constructor parameters directly when inheriting in contract signature
contract U is S("s"), T("t") {

}

// if the constructor inputs are dynamic then we can include them in the constructor signature of the child contract
contract V is S, T {
    constructor(string memory _name, string memory _text) S(_name) T(_text) {

    }
}

// combination of both of the above can also be used
contract VV is S("s"), T {
    constructor(string memory _text) T(_text) {
        
    }
}