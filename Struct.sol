// using named parameters we can change the order of input variables while initializing the struct
// struct can also be initialized like initializing values in a javascript object using dot (.) operator
// we can also use delete keyword with structs we can delete a specific field or an entire struc but keep in mind that
// delete will only set the value to default value of the respective data type

// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


contract Structs {

    struct Car {
        string model;
        uint year;
        address owner;
    }

    Car[] public cars;

    mapping(address => Car[]) public carsByOwner;

    function examples() external {

        // First way of initialization of struct
        // Car memory toyota = Car("Toyota", 1999, msg.sender);
        
        // Second way of initialization of struct
        // Car memory toyota = Car({owner: msg.sender, model: "Toyota", year: 1990});
        
        // Third way of initialization of struct
        Car memory toyota;
        toyota.owner = msg.sender;
        toyota.model = "Toyota";
        toyota.year = 1990;

        Car memory lambo = Car("Lamborigni", 2002, msg.sender);

        cars.push(toyota);
        cars.push(lambo);

        // initializing struct and at the same time pushing in to array
        cars.push(Car("Toyota", 2010, msg.sender));

        Car storage _car = cars[0];

        _car.year = 2010;







    }




}